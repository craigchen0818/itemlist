import { Component, EventEmitter, Input, Output, ViewChild } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Items } from 'src/items-interface';
import { InputTextModule } from 'primeng/inputtext';
import { DialogModule } from 'primeng/dialog';
import { ButtonModule } from 'primeng/button';
import { TableModule } from 'primeng/table';
import { FormsModule } from '@angular/forms';
import { AutoCompleteModule } from 'primeng/autocomplete';

@Component({
  selector: 'his-item-list',
  standalone: true,
  imports: [CommonModule,AutoCompleteModule,
    ButtonModule,DialogModule,InputTextModule,
    TableModule,FormsModule],
  templateUrl: './item-list.component.html',
  styleUrls: ['./item-list.component.scss']
})
export class ItemListComponent {
  @Input() items!:Array<Items[]>;
  @Output() item : EventEmitter<string> = new EventEmitter<string>();

  visible: boolean = true;
  searchText:string = '';

  showDialog() {
      this.visible = true;
  }

  returnItem(value:string){
    this.item.emit(value);
  }

  itemListArray:Items[] = [
    {
      keys:"BID",
      values:"一日兩次"
    },
    {
      keys:"BID&HS",
      values:"早晚飯後及睡前"
    },
    {
      keys:"BIW",
      values:"每週兩次"
    },
    {
      keys:"CM",
      values:"翌日"
    },
  ];
  tempItemListArray=[...this.itemListArray];

  // selectedItem:any;

  // search($event: any){
  //   console.log($event);
  // }



}
